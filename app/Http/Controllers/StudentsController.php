<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class StudentsController extends Controller
{
    public function show(Request $request){

        //$action = $_POST['action'] ?? '';
        $action = $request->input('action');
        if($action!='insert')$id = $request->input('id');

        $name = $request->input('name');
        $surname = $request->input('surname');
        $find_text = $request->input('find_text');
        if ($action == 'edit') {
//            $affected = DB::update("UPDATE tbstudents SET name='$name', surname='$surname' WHERE id='$id'");
            $affected = DB::table('tbstudents')->where('id',$id)->update(['name'=>$name,'surname'=>$surname]);

        }

        if ($action == 'delete') {
            $deleted = DB::delete("DELETE FROM tbstudents WHERE id='$id'");
        }
        if($action=='insert'){
            $age=$request->input('age');
            $grp = $request->input('group_name');
            DB::table('tbstudents')->insert(['name'=>$name,'surname'=>$surname,'age'=>$age,'group_name'=>$grp]);
        }
        if($action=='find'){
          $student_array=  DB::table('tbstudents')->where('name','like','%'. $find_text . '%')->orWhere('surname','like','%'. $find_text . '%')->get();
        }else{
            $student_array = DB::table('tbstudents')->get();

        }
      //  $student_array = DB::select('SELECT * FROM students');

        //dd($student_array);
        return view('student_show', ['student_array' => $student_array]);
    }

    // public function edit(){
    //     return view('student_edit');
    // }


}
