<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// home page
use Illuminate\Support\Facades\Route;



Route::get('/', function () {
    return view('welcome');
});


Route::get('students', 'StudentsController@show');
Route::post('students', 'StudentsController@show');

Route::get('student_edit', 'StudentsController@edit');


// page http://laravel/hello
Route::any('hello/{name?}', function ($name = 'Joha SQL') {
    return 'Hello '.$name;
});


