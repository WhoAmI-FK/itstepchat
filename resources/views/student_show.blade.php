<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <style>
        table{
            border-collapse: collapse;
        }
    </style>
</head>
<body>

    <h3> Student List</h3>


<table border="1">
    <tr>
        <th>№</th>
        <th>Name Surname</th>
        {{-- <th>Surname</th> --}}
        <th>Age</th>
        <th> </th>
    </tr>

    @foreach ($student_array as $student)
        <tr>
            <td>{{ $loop->iteration }}</td>
            <td>
                {{ $student->name }}  &nbsp;{{ $student->surname }} <br>

                <form action="" method="POST">
                    @csrf
                    name:<input type="text" name='name' style="width: 60px">
                    &nbsp;
                    surname:<input type="text" name='surname' style="width: 60px">
                    <input type="hidden" name="id" value="{{ $student->id }}">
                    <input type="hidden" name="action" value="edit">
                    <input type="submit" value="save">
                </form>

            </td>
            <td>{{ $student->age }}</td>
            <td>
                <form action="" method="POST">
                    @csrf
                    <input type="submit" value="Del">
                    <input type="hidden" name="id" value="{{ $student->id }}">
                    <input type="hidden" name="action" value="delete">
                </form>
            </td>

        </tr>
    @endforeach
   <tr>
    <td>
        <form style="display:flex;flex-direction:column;" action="" method="POST">
            @csrf
            <h2>New user</h2>
            <input type="text" name="name" placeholder="name..." />
            <input type="text" name="surname" placeholder="surname..." />
            <input type="number" name="age" placeholder="age"/>
            <input type="text" name="group_name" placeholder="group name..."/>
            <input type="submit"/>
            <input type="hidden" name="action" value="insert">
        </form>
    </td>
    <td>
        <form style="display:flex;flex-direction:column;" action="" method="POST">
            @csrf
            <h2> Find:</h2>
            <input type="text" name='find_text'/>
            <input type="hidden" name="action" value="find">
            <input type="submit" value="Find"/>
        </form>
    </td>
</tr>
</table>
<br>

@php
echo 'Students Count : '.count($student_array);
@endphp






</body>
</html>
